# Plist Browser
A library to create and read Plist files in the browser.

## What is this?
This repository is mainly for reading (and generating) plist-XML files in te browser, however, it has some Apple-related libraries that also run in the browser.
